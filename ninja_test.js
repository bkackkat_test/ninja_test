
var WebSocket = require('ws'),
    ws = new WebSocket("ws://nuclear.t.javascript.ninja");

var globalStateId = '180000';
var leverArr = [
                ['_', '_', '_', '_'],
                ['_', '_', '_', '_'],
                ['_', '_', '_', '_'],
                ['_', '_', '_', '_']
               ];

var allLeverSets = false;

console.log('^^^^^^^^ Display FIRST ^^^^^^^^^^^');
displayCurrentLeversState();

ws.on('open', function()
{
    console.log("Соединение установлено.");
});

ws.onclose = function(event)
{
    if (event.wasClean) {
        console.log('Соединение закрыто чисто');
    } else {
        console.log('Обрыв соединения'); // например, "убит" процесс сервера
    }
    console.log('Код: ' + event.code + ' причина: ' + event.reason);
};

ws.onerror = function(error)
{
    console.log("Ошибка " + error.message);
};

ws.onmessage = function(event)
{
    console.log("Получены данные " + event.data);

    var jsonObject = JSON.parse(event.data);

    if (jsonObject.action === 'check')
    {
        changePairLever(jsonObject);
    }
    else if (jsonObject.pulled != '')
    {
        var currStateId = jsonObject.stateId;
        var currLeverId = jsonObject.pulled;

        inverseState(currLeverId);

        console.log(currStateId);

        if (currStateId != globalStateId)
        {
            if (allLeverSets)
            {

            }
            else
            {
                checkLeversPosition(currLeverId, currStateId);
            }

            globalStateId = currStateId;
        }
    }

    console.log('^^^^^^^^ Display AFTER ^^^^^^^^^^^');
    displayCurrentLeversState();
};

function displayCurrentLeversState ()
{
    console.log('Current levers position:' );
    for (var i = 0; i <= 3; i++)
    {
        console.log(leverArr[i]);
    }
}

function checkLeversPosition (pulledLever, currentState)
{
    var currLever = pulledLever;
    var nextLever = pulledLever + 1;

    while ( nextLever != pulledLever )
    {
        if (nextLever > 3)
            nextLever = 0;
        if (leverArr[currLever][nextLever] === "_")
        {
            ws.send( JSON.stringify(
                { action: "check" ,
                    "lever1" : currLever,
                    "lever2" : nextLever,
                    stateId: currentState })
            );
            break;
        }
        nextLever++;
    }
}

function inverseState (leverId)
{
    if (leverArr[leverId][leverId] === 'X')
        leverArr[leverId][leverId] = 'O';
    else
        leverArr[leverId][leverId] = 'X';

    checkAllMatrixSet();
}

function checkAllMatrixSet()
{
    if (!allLeverSets)
    {
        var j = 0;

        for (var i = j; i <= 3; i++)
        {
            console.log('j =', j, ' i =', i);

            if (leverArr[j][i] === '_')
                break;

            if ((i === 3) && (j != 3))
            {
                j++;
                i = j;
            }

        }
        if ((i >= 3) && (j >= 3))
        {
            allLeverSets = true;
            console.log('All levers are set to some state');
        }
    }
}

function inversedState(leverId)
{
    if (leverArr[leverId][leverId] === 'X')
        return 'O';
    else
        return 'X';
}

function changePairLever(jsonObject)
{
    var lever1 = jsonObject.lever1;
    var lever2 = jsonObject.lever2;

    if (jsonObject.same === true)
    {
        leverArr[lever2][lever2] = leverArr[lever1][lever1];
        leverArr[lever1][lever2] = 's';
        leverArr[lever2][lever1] = 's';

        console.log('+++ Lever' + lever2 + ' set in same state as Lever' + lever1 + ' = ' + leverArr[lever1][lever1]);
    }
    if (jsonObject.same === false)
    {
        leverArr[lever2][lever2] = inversedState(lever1);
        leverArr[lever1][lever2] = 'i';
        leverArr[lever2][lever1] = 'i';

        console.log('--- Lever' + lever2 + ' set in inverse state of Lever' + lever1 + ' = ' + leverArr[lever1][lever1]);
    }

    console.log('^^^^^^^^ Display BEFORE relationsCheck  ^^^^^^^^^^^');
    displayCurrentLeversState();

    checkRelations(jsonObject);

    console.log('^^^^^^^^ Display AFTER relationsCheck  ^^^^^^^^^^^');
    displayCurrentLeversState();


}

function checkRelations(jsonObject)
{
    var lever1 = jsonObject.lever1;
    var lever2 = jsonObject.lever2;

    for (var i = 0; i <= 3 ; i++)
    {
        console.log('>>> Current iteration: Lever1 = ', lever1, ' lever2 = ', lever2, ' i = ', i);
        if ((leverArr[lever1][i] === '_') || (i === lever1)|| (i === lever2))
            continue;

        if (leverArr[lever1][i] === 's')
        {
            leverArr[i][i] = leverArr[lever1][lever1];
            console.log('+++> Lever' + i + ' set in same state as Lever' + lever1 + ' = ' + leverArr[lever1][lever1]);
        }
        if (leverArr[lever1][i] === 'i')
        {
            leverArr[i][i] = inversedState(lever1);
            console.log('---> Lever' + i + ' set in inverse state of Lever' + lever1 + ' = ' + leverArr[lever1][lever1]);

        }
    }
}
/*
 ws.on('message', function(message)
 {
 console.log('received: %s', message);
 });
 */